// Program that reads the temperature

#include <OneWire.h>
#include <DS18B20.h>

// PIN Number for the data input
#define ONEWIRE_PIN 10

// Adres czujnika
byte address[8] = {0x28, 0xAA, 0x44, 0x25, 0x3D, 0x14, 0x01, 0xCD};

OneWire onewire(ONEWIRE_PIN);
DS18B20 sensors(&onewire);

void setup() {
  while(!Serial);
  Serial.begin(9600);

  sensors.begin();
  sensors.request(address);
}

void loop() {
  if (sensors.available())
  {
    float temperature = sensors.readTemperature(address);

    Serial.println(F("'C: "));
    Serial.print(temperature);

    sensors.request(address);
  }

  // further instructions
}

