/*
MS5535C Pressure Sensor calwords readout
This program will read your MS5535C or compatible pressure sensor every 5 seconds and show you the calibration words, the calibration factors,
the raw values and the compensated values of temperature and pressure.
Once you read out the calibration factors you can define them in the header of any sketch you write for the sensor.

Pins:
MS5535C sensor attached to pins 10 - 13:
DIN (MOSI): pin 11
DOUT (MISO): pin 12
SCLK: pin 13
MCLK: pin 9

Created August 2011 by SMStrauch based on application note AN510 from www.intersema.ch (http://www.meas-spec.com/downloads/Using_SPI_Protocol_with_Pressure_Sensor_Modules.pdf),
and with help of robtillaart and ulrichard.

Modified February 2012 by spincrisis for the MS5535C.
*/

// include library:
#include <SPI.h>

// generate a MCKL signal pin
const int clock = 9;

void resetsensor() //this function keeps the sketch a little shorter
{
  SPI.setDataMode(SPI_MODE0);
  SPI.transfer(0x15);
  SPI.transfer(0x55);
  SPI.transfer(0x40);
}

void setup() {
  Serial.begin(9600);
  SPI.begin(); //see SPI library details on arduino.cc for details
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV32); //divide 16 MHz to communicate on 500 kHz
  pinMode(clock, OUTPUT);
  delay(100);
}

void loop()
{
  TCCR1B = (TCCR1B & 0xF8) | 1 ; //generates the MCKL signal
  analogWrite (clock, 128) ;

  long c1 = 22720;
  long c2 = 1912;
  long c3 = 716;
  long c4 = 612;
  long c5 = 1011;
  long c6 = 23;

  resetsensor();//resets the sensor

  //Temperature:
  unsigned int tempMSB = 0; //first byte of value
  unsigned int tempLSB = 0; //last byte of value
  unsigned int D2 = 0;
  SPI.transfer(0x0F); //send first byte of command to get temperature value
  SPI.transfer(0x20); //send second byte of command to get temperature value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  tempMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  tempMSB = tempMSB << 8; //shift first byte
  tempLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D2 = tempMSB | tempLSB; //combine first and second byte of value
  Serial.println("Temperature2 raw:");
  Serial.println(D2); //voilá!

  resetsensor();//resets the sensor

  //Pressure:
  unsigned int presMSB = 0; //first byte of value
  unsigned int presLSB =0; //last byte of value
  unsigned int D1 = 0;
  SPI.transfer(0x0F); //send first byte of command to get pressure value
  SPI.transfer(0x40); //send second byte of command to get pressure value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  presMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  presMSB = presMSB << 8; //shift first byte
  presLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D1 = presMSB | presLSB; //combine first and second byte of value
  Serial.println("Pressure raw:");
  Serial.println(D1);

  //calculation of the real values by means of the calibration factors and the maths
  const double UT1 = (c5 << 3) + 20224;
  const double dT = D2 - ((8.0 * c5) + 20224.0);
  //const long TEMP = 200 + dT * (c6 + 50) >> 10;
  const double OFF  = c2 * 4.0         + (  (   ( c4-512.0) *  dT ) / 4096.0);
  const double SENS = 24576.0 +  c1    + (  (   c3 *  dT ) / 1024.0);
  const double X = (( SENS * (D1- 7168.0)) / 16384.0) -OFF;
  double PCOMP = 250.0 +   X / 32;    //(SENS * (D1 - OFF) >> 12) + 1000;
  double TEMP = 20.0 +      ( (  dT * ( c6+50.0) ) / 10240.0);
  float TEMPREAL = TEMP/10;
 

  Serial.println("Temperature2:");
  Serial.println(TEMP);

  Serial.println("Pressure:");
  Serial.println(PCOMP);

  //2nd order compensation only for T > 0°C
  //const long dT2 = dT - ((dT >> 7 * dT >> 7) >> 3);
  //const float TEMPCOMP = (200 + (dT2*(c6+100) >>11))/10;
  //Serial.print("2nd order compensated temperature in °C =");
  //Serial.println(TEMPCOMP); 

  delay(5000);
}
