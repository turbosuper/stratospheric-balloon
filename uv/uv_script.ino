int uvSensorIn1 = A5;
int uvSensorIn2 = A4;
int uvSensorIn3 = A3;

int UVindexByMV(int mv){
 if (mv < 227){
  return 0;
 } else if ((mv >= 227) && (mv < 318)){
  return 1; 
 } else if ((mv >= 318) && (mv < 408)){
  return 2; 
 } else if ((mv >= 408) && (mv < 503)){
  return 3;
 } else if ((mv >= 503) && (mv < 606)){
  return 4;
 } else if ((mv >= 606) && (mv < 696)){
   return 5;
 } else if ((mv >= 696) && (mv < 795)){
   return 6;
 } else if ((mv >= 795) && (mv < 881)){
   return 7;
 } else if ((mv >= 881) && (mv < 976)){
   return 8;
 } else if ((mv >= 976) && (mv < 1070)){
   return 9;
 } else {
   return 10;
 }
}

void UVsetup(){
  pinMode(uvSensorIn1, INPUT); 
  pinMode(uvSensorIn2, INPUT);
  pinMode(uvSensorIn3, INPUT);
  analogReference(INTERNAL);
}

void UVloop(){
  const int sensors[3] = {uvSensorIn1, uvSensorIn2, uvSensorIn3};
 for (unsigned int i = 0; i < 3; i++){
  int uvValue = analogRead(sensors[i]);
  String prefix = "UV";
  prefix += (i+1);
  prefix += ".volt:";
  Serial.println(prefix);
  Serial.println(uvValue);
  prefix = "UV";
  prefix += (i+1);
  prefix += ".index:";
  Serial.println(prefix);
  Serial.println(UVindexByMV(uvValue));
 } 
} 
void setup(){
  UVsetup();
  Serial.begin(9600);
}
 
void loop(){
  UVloop();
  Serial.println("--");
  delay(2000);
}


