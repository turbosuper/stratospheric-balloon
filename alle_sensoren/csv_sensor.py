#!/usr/bin/env python
# -*- coding: utf8 -*-
import serial
import sys
from datetime import datetime
from shutil import copyfile
i = 1
ser = serial.Serial('/dev/ttyUSB0', 9600)
ser.readline()
output = ""
now = datetime.now()
filename = "/home/pi/ballon/outputs/"
filename += now.strftime("%d-%m-%Y_%H-%M-%S")
filename += ".csv"
ofh = open(filename, "w")
ofh.write("Time,GM Counter,uv0.volt,uv0.index,uv1.volt,uv1.index,uv2.volt,uv2.index,Temprature.Temp,Pressure.TempRAW,Pressure.PressureRAW,Pressure.Temp,Pressure.Pressure\n")
ofh.close()
while i == 1:
	line = ser.readline()
	line = line[ : len(line)-2]
	if line == "--":
		copyfile(filename, filename + ".safety")
		ofh = open(filename, "a")
		ts = datetime.now()
		ts_str = ts.strftime("%d.%m.%Y %H:%M:%S")
		ofh.write(ts_str)
		output += '\n'
		ofh.write(output)
		ofh.close()
		output = ""
	elif line.endswith(":"):
		print("Skipped line")# this code line is need - elif to handle prefix outputs - print bec. there must be code	
	elif line.endswith("XX"):
		i = 0
	else:
		output += ',' + line

ser.close()
