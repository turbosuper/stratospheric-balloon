![](/40000meter.jpg)

Code for sensors mounted on stratospheric balloon. Sensors are connected to 
arduino, and raspberry pi as a control unit.
Following chrasteristics are measured:
- gamma radiation
- temperature
- air pressure
- uv radiation
Pictures were being taken every 6 minutes.

The balloon reached 40 km. The results are in /Daten der PIs/

General documentation, and raspberry's code is reported in 
/Dokumentation/Dokumentation Stratosphare Ballon.pdf

Single sensors' arduino's code and and assembly instructions are documented 
in /Dokumentation/{Sensor name}.pdf