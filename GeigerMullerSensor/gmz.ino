
unsigned long counts;
unsigned long currMillis;
unsigned long prevMillis;

void impulse(){
  counts++;
}

#define LOG_PERIOD 60000 // count rate
void setup() {
  counts = 0;
  prevMillis = 0;
  Serial.begin(9600);
  pinMode(2, INPUT);
  attachInterrupt(digitalPinToInterrupt(2), impulse, FALLING);//aktiv wenn Flanke fällt. zählt counts +1
  Serial.println("Start counter");
}


void loop() {
  currMillis = millis();
  if( LOG_PERIOD <= (currMillis - prevMillis) && millis() >= 1){
  //if ((millis()+1)%LOG_PERIOD==0) {
    prevMillis = currMillis;
    Serial.print(counts);
    Serial.print(" -> Zeit: ");
    Serial.print(currMillis/60000);
    if(currMillis/60000 == 1){
      Serial.print(" Minute ");
    }else Serial.print(" Minuten ");
    Serial.print((currMillis-prevMillis));
     if(currMillis/60000 == 1){
      Serial.println(" Millisekunden");
    }else Serial.println(" Millisekunden");
    counts = 0;
  }
}


//counts pro Minute
//void loop() {
//  unsigned long currentMillis = millis();
//  seconds = millis()/1000;
//  if (curentMillis - previousMillis > LOG_PERIOD) {
//    previousMillis = currentMillis;
//    Serial.println(counts); //
//    counts = 0;
//  }
//}
